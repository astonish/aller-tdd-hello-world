// Import dependencies
import Should from 'should'

// Import function
import add from './add.method'

// Tests
describe('add', () => {
  
  // Positive tests (including boundary testing)
  
  it('should add integers', () => {
    // Arrange
    const a = 18, b = 24

    // Act
    const result = add(a, b)

    // Assert
    result.should.equal(42)
  })
  
  it('should add floats', () => {
    // Arrange
    const a = 18.13, b = 24.0037

    // Act
    const result = add(a, b)

    // Assert
    result.should.equal(42.1337)
  })

  it('should add zeroes', () => {
    // Arrange
    const a = 0, b = 0

    // Act
    const result = add(a, b)

    // Assert
    result.should.equal(0)
    
  })

  it('should add negative numbers', () => {
    // Arrange
    const a = -18, b = -24

    // Act
    const result = add(a, b)

    // Assert
    result.should.equal(-42)
  })

  // Negative tests

  it('should throw an error if invoked without parameters', () => {
    // Arrange
    // -

    // Act
    (() =>
      add()

    // Assert
    ).should.throw(new Error(`Invalid parameters`))

  })

  it('should throw an error if invoked with empty parameters', () => {
    // Arrange
    const a = null, b = undefined;

    // Act
    (() =>
      add(a, b)

    // Assert
    ).should.throw(new Error(`Invalid parameters`))
  })

  it('should throw an error if invoked with invalid parameters', () => {
    // Arrange
    const a = "hi", b = "this is silly";

    // Act
    (() =>
      add(a, b)

    // Assert
    ).should.throw(new Error(`Invalid parameters`))
  })

})
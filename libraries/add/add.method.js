
// Simple add method
const add = (a, b) => {
  if (isNaN(a) || isNaN(b)) {
    throw new Error(`Invalid parameters`)
  }
  
  return a + b
}

// Export method
export default add
// Import dependencies
import Should from 'should'

// Import function
import divide from './divide.method'

// Tests
describe('divide', () => {
  
  // Positive tests (including boundary testing)
  
  it('should divide integers', () => {
    // Arrange
    const a = 294, b = 7

    // Act
    const result = divide(a, b)

    // Assert
    result.should.equal(42)
  })
  
  it('should divide floats', () => {
    // Arrange
    const a = 134.82784, b = 3.2

    // Act
    const result = divide(a, b)

    // Assert
    result.should.equal(42.1337)
  })

  it('should handle zero as numerator', () => {
    // Arrange
    const a = 0, b = 7

    // Act
    const result = divide(a, b)

    // Assert
    result.should.equal(0)
    
  })

  it('should divide negative numbers', () => {
    // Arrange
    const a = -378, b = -9

    // Act
    const result = divide(a, b)

    // Assert
    result.should.equal(42)
  })

  // Negative tests

  it('should throw an error if invoked zero as denominator', () => {
    // Arrange
    const a = 7, b = 0;

    // Act
    (() =>
      divide(a, b)

    // Assert
    ).should.throw(new Error(`Universe implosion avoided`))
  })

  it('should throw an error if invoked without parameters', () => {
    // Arrange
    // -

    // Act
    (() =>
      divide()

    // Assert
    ).should.throw(new Error(`Invalid parameters`))

  })

  it('should throw an error if invoked with empty parameters', () => {
    // Arrange
    const a = null, b = undefined;

    // Act
    (() =>
      divide(a, b)

    // Assert
    ).should.throw(new Error(`Invalid parameters`))
  })

  it('should throw an error if invoked with invalid parameters', () => {
    // Arrange
    const a = "hi", b = "this is silly";

    // Act
    (() =>
      divide(a, b)

    // Assert
    ).should.throw(new Error(`Invalid parameters`))
  })

})
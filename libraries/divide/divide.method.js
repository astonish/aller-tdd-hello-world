
// Simple divide method
const divide = (a, b) => {
  if (isNaN(a) || isNaN(b)) {
    throw new Error(`Invalid parameters`)
  }

  if (b === 0) {
    throw new Error(`Universe implosion avoided`)
  }
  
  return a / b
}

// Export method
export default divide
### Server

#### Installation

To install server dependencies execute the following command

    npm install

#### Test

To run the test suite execute the following command

    npm test
    - or -
    npm run test-coverage